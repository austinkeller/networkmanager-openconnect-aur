# Maintainer: Jan Alexander Steffens (heftig) <heftig@archlinux.org>
# Contributor: Ionut Biru <ibiru@archlinux.org>

pkgname=networkmanager-openconnect
pkgver=1.2.7dev+65+gca4187c
pkgrel=1
pkgdesc="NetworkManager VPN plugin for OpenConnect"
url="https://wiki.gnome.org/Projects/NetworkManager"
arch=(x86_64)
license=(GPL)
depends=(libnm libsecret openconnect libopenconnect.so)
makedepends=(libnma intltool python git)
optdepends=('libnma: GUI support')
_commit=6798cf337a6381c80b70e4bef4fa6e0c3d9baf21  # add-base-mtu-property
source=("git+https://gitlab.gnome.org/austinkeller/NetworkManager-openconnect.git#commit=$_commit")
sha256sums=('SKIP')

pkgver() {
  cd NetworkManager-openconnect
  git describe --tags | sed 's/-dev/dev/;s/-/+/g'
}

prepare() {
  cd NetworkManager-openconnect
  intltoolize --automake --copy
  autoreconf -fvi
}

build() {
  cd NetworkManager-openconnect
  ./configure --prefix=/usr --sysconfdir=/etc --localstatedir=/var \
    --libexecdir=/usr/lib --disable-static
  sed -i -e 's/ -shared / -Wl,-O1,--as-needed\0/g' libtool
  make
}

package() {
  cd NetworkManager-openconnect
  make DESTDIR="$pkgdir" install dbusservicedir=/usr/share/dbus-1/system.d
  echo 'u nm-openconnect - "NetworkManager OpenConnect"' |
    install -Dm644 /dev/stdin "$pkgdir/usr/lib/sysusers.d/$pkgname.conf"
}

# vim:set sw=2 et:
